<!-- no page number for one page -->
\thispagestyle{empty}

# Interns Lightning Talks

### August 7, 2015

#. Ankit:
:   GPS Sharing
#. Georges Basile Stavracas Neto:
:   Add Drive dialog for Nautilus
#. Rashi Aswani:
:   Automated Tests for Gnome-logs
#. Jonathan Kang:
:   Gnome-Logs
#. Adrien Plazas:
:   Boxes - Hardening Sprint, GtkAssistant and MIME-types Integration
#. Andrei Gabriel Macavei:
:   GnomeKeysign
#. Abdeali Kothari:
:   Gedit Plugin for coala
#. Udayan Tandon:
:   GUI for coala
#. Bastian Ilsø Hougaard:
:   Enhance the Chatting Experience in Polari
#. Timm Bäder:
:   Add an Image Viewing Widget to GTK+
#. Marcin Kolny:
:   Refactor a Gstdataprotocol Library and Create GStreamer's Debugger
#. Oliver Luo:
:   Evolution Configuration EExtension for ActiveSync
#. George-Cristian Muraru:
:   Disable USB on Lockscreen
#. Iulian-Gabriel Radu:
:   Nibbles: Modernization
#. Alessandro Bono:
:   A New Collections Dialog for Documents
#. Magdalen Berns:
:   Develop Java ATK wrapper
