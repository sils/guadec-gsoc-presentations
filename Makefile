default: presentations order

presentations:
	gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=finished.pdf 0*.pdf

order:
	pandoc order.md -o order.pdf -V geometry:margin=1.3in
